// console.log("Elo word!");


// [1]IF/ELSE statement

// Executes a statement if a specified condition is true. 

let numA = 0;

if(numA < 0 ){
	console.log("Hello");
}
else{
	console.log("Hiiiiii");
}

if(numA == 0){
	console.log("EHMERGERD")
}


let city = "NY";

if(city=="NY"){
	console.log("Welcome to NY City");
}

//------------------------------------------------------

const numH = 0;

if(numH < 0){
	console.log("OKAY");
}
else if(numH > 0){
	console.log("OWKAY");
}
else{
	console.log("THIS IS CORRECT");
}

//------------------------------------------------------


if(city === "NY"){
	console.log("Welcum to New Nyork City!")
}
else{
	console.log("Welcome, idc bout you boy");
}


let username ="admin";
let password = "admin123"


if(username == "admin" && password == "admin1234"){
	console.log("Correct Username and Password");
}
else if(username == "admin" || password == "admin1234"){
	console.log("Wrong Username or Password, Try again.");
}
else{
	console.log("You dumbass");
}


// ------------------------------------

let message = "No message";
	console.log(message);

	function determineTyphooneIntensity(windSpeed){

		if(windSpeed < 30){
			return "Not a typhoon yet."
		}
		else if(windSpeed <= 61){
			return "Tropical depression detected."
		}
		else if (windSpeed >= 62 && windSpeed <= 88){
			return "Tropical storm detected."
		}
		else if(windSpeed >= 89 && windSpeed <= 117){
			return "Severe Tropical storm detected";
		}
		else{
			return "Typhoon detected."
		}
	}

	message = determineTyphooneIntensity(110);
	console.log(message);

	//  console.warn() is a good way to print warnings in our console that could help us developers act on certain output within our code.
	if(message === "Severe Tropical storm detected"){
		console.warn(message);
	}


// Truthy or False - in javascript a truthy value that is considered true when encountered in a boolean Context
// -False
// 	1. false
// 	2. 0
//  3. ""
//  4. null
//  5. undefined
//  6. NaN  


//Truthy fruit

let isMarried = true;


if(true){
	console.log("Truthy");
}
if(1){
	console.log("Truthy");
}
if([]){
	console.log("Truthy");
}

if(false){
	console.log("Falsy");
}
if(0){
	console.log("Falsy");
}
if(undefined){
	console.log("Falsy");
}

if(isMarried){
	console.log("Truthy");
}


// [2]Conditional (Ternary) Operator
/*
	- takes in 3 operands/ alternative format to if/else
*/

let ternaryResult = (1 < 18)? true : false;
console.log("Result of Ternary Operator: " + ternaryResult);

// Multi statement execution using ternary operator

let name;

function isOfLegalAge(){
	name = "John";
	return "You are in legal age,";
}

function underAge(){
	name = "Jane"
	return "You are under the age limit,";
}

let age = parseInt(prompt("What is your age"));
console.log(age);

let legalAge = (age>=18)? isOfLegalAge() : underAge();
console.log("Return of Ternary Operator in functions: "+ legalAge +" "+ name);





///[3]SWITCH Statements

/*let day = prompt("What day of the week is it today").toLowerCase();
console.log(day);

switch(day){
	case "monday":
		console.log("The color of the day is red");
		break;
	case "monday":
		console.log("The color of the day is blue");
		break;
	case "wednesday":
		console.log("The color of the day is nyelo");
		break;
	default:
		console.log("Please input a valid day");
		break;
}*/

let yourAge = prompt("Age: ");
yourAge = parseInt(yourAge);

switch(yourAge){
	case yourAge > 18:
		console.log("You are safe.");
	case yourAge = 18:
		console.log("Give freebies");
}


// Try-Catch-Finally Statement

let codeErrorMessage = "Code Error";

function showIntensityAlert(windSpeed){
	try{
		//code block to try
		alert(determineTyphooneIntensity(windSpeed));
	}
	catch(error){
		//error.message is used to access the info relating to an error object
		console.warn(error.message);
	}
	finally{
		alert("Intensity updates will show new alert. ")
	}

}

showIntensityAlert(110);

console.log("Sample output after try-catch-finally block");